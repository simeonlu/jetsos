//
//  DetailViewController.m
//  Jetsos
//
//  Created by Simeon on 10/09/2015.
//  Copyright (c) 2015 CathayPacific. All rights reserved.
//

#import "DetailViewController.h"
#import "YTPlayerView.h"

@interface DetailViewController () <YTPlayerViewDelegate,UIWebViewDelegate>
@property(nonatomic,strong)UIAlertView *alert;
@property(nonatomic,strong)IBOutlet YTPlayerView *ytView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spiner;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subTitleL;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *markL;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@end

//static  NSString* const videoID = @"QcHJpaETHEw";

static  NSString* const videoID = @"8H5fhUGkCwI";//@"0JiA-uHBARI";// @"PLhBgTdAWkxeCMHYCQ0uuLyhydRJGDRNo5";

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Details";
    self.ytView.delegate = self;
   // self.webview.hidden = YES;
    
    self.titleL.alpha = 0;
    self.subTitleL.alpha = 0;
    self.icon.alpha = 0;
    self.markL.alpha = 0;
    self.textView.alpha = 0;
    
    //self.webview.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //[_ytView loadVideoById:videoID startSeconds:15 suggestedQuality:kYTPlaybackQualityAuto];
    [_ytView loadWithVideoId:videoID playerVars:@{@"playsinline" : @1}];

    
}

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    [self.spiner stopAnimating];
    [UIView animateWithDuration:0.3 delay:2.0 options:UIViewAnimationOptionCurveEaseInOut  animations:^{
        self.titleL.alpha = 1;
        self.subTitleL.alpha = 1;
        self.icon.alpha = 1;
        self.markL.alpha = 1;
        self.textView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    [self.ytView playVideo];
    
}
- (void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error {
    NSLog(@"error:%@",@(error));
    
}


//-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//    NSLog(@"error when load web view:%@",error.debugDescription);
//}
//
//-(void)webViewDidFinishLoad:(UIWebView *)webView{
//     [self.spiner stopAnimating];
//}
//

@end
