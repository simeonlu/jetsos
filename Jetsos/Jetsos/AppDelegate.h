//
//  AppDelegate.h
//  Jetsos
//
//  Created by Simeon on 10/09/2015.
//  Copyright (c) 2015 CathayPacific. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

