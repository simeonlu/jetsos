//
//  ViewController.m
//  Jetsos
//
//  Created by Simeon on 10/09/2015.
//  Copyright (c) 2015 CathayPacific. All rights reserved.
//

#import "ViewController.h"
#import "DetailViewController.h"
#import "AFNetworking.h"


@interface ViewController () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) IBOutlet UITableView *favouritesList;
@property (weak, nonatomic) IBOutlet UIImageView *sideIV;
@property (nonatomic, strong) NSArray* picNames;
@property (nonatomic, strong) NSTimer *timer;
@end


static NSString *const STAGEKEY =@"Stage";
static NSString *const VALUEKEY =@"Value";


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.title = @"My favourite list";
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    self.picNames = @[@"app-gem-darjeeling",@"app-gem-salzburg",@"app-gem-suzhou",@"app-gem-yosemite"];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(fireListerner) userInfo:nil repeats:YES];
}

#pragma mark - tableview delegate / tableview datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _picNames.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JScell *cell = [[JScell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass(self.class)];
    
    NSString *name = _picNames[indexPath.row];

    UIImage *img = [UIImage imageNamed:name ];
    cell.iv.image = img;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row > 0) {
        return;
    }
    DetailViewController *vc = [[DetailViewController alloc]initWithNibName:@"DetailViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160.f;
}

#pragma mark - timer triger

-(void)fireListerner {
    
    NSLog(@"timer is firing");
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://119.9.110.209:8080/saved" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:NSArray.class]) {
             NSLog(@"JSON: %@", responseObject);
            NSArray *result = (NSArray*)responseObject;
            if (result.count) {
                NSDictionary*dict = result[0];
                NSString *stage = dict[STAGEKEY];
                NSString*value = dict[VALUEKEY];
                if (value.boolValue && [stage isEqualToString:@"saved"]) {
                    [self addNewFavourite];
                }
            }else {
                [self clearNewFavourite];
            }
        }
 
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

-(void)addNewFavourite {

    if ([_picNames containsObject:@"app-gem-greenisland"]) {
        return;
    }
    self.picNames = @[@"app-gem-greenisland",@"app-gem-darjeeling",@"app-gem-salzburg",@"app-gem-suzhou",@"app-gem-yosemite"];
    [self.favouritesList reloadData];
}

-(void)clearNewFavourite {
    
    if (_picNames.count>4) {
        self.picNames = @[@"app-gem-darjeeling",@"app-gem-salzburg",@"app-gem-suzhou",@"app-gem-yosemite"];
        [self.favouritesList reloadData];
    }

}






@end


@implementation  JScell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _iv = [[UIImageView alloc]initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:_iv];
    }
    return self;
}


-(void)layoutSubviews {
    [super layoutSubviews];
    self.iv.frame = self.contentView.frame;
}
@end



